/* -*- Mode: C++; -*- */
// C&C NLP tools
// Copyright (c) Universities of Edinburgh, Oxford and Sydney
// Copyright (c) James R. Curran
//
// This software is covered by a non-commercial use licence.
// See LICENCE.txt for the full text of the licence.
//
// If LICENCE.txt is not included in this distribution
// please email candc@it.usyd.edu.au to obtain a copy.

namespace NLP {
  namespace CCG {

    class SuperCat;
    class Chart;
    class kBestSC;

    class Decoder {
    public:
      Decoder(void){}
      virtual ~Decoder(void){ /* do nothing */ }

      virtual double best_score(const SuperCat *sc) = 0;
      const SuperCat *best_equiv(const SuperCat *sc);
      virtual std::vector<kBestSC> *best(Chart &chart);
      
      std::vector<kBestSC> k_merge_0(std::vector<kBestSC> *a, std::vector<kBestSC> *b, const double K);
      
      virtual std::vector<kBestSC> k_best_0_score(const SuperCat *sc, const double K) = 0;
      std::vector<kBestSC> *k_best_0_equiv(const SuperCat *sc, const double K);
      virtual std::vector<kBestSC> *k_best_0(Chart &chart);
      
      virtual std::vector<kBestSC> k_best_1_score(const SuperCat *sc, const double K) = 0;
      std::vector<kBestSC> *k_best_1_equiv(const SuperCat *sc, const double K);
      virtual std::vector<kBestSC> *k_best_1(Chart &chart); 
      
      virtual kBestSC k_best_2_score(const SuperCat *sc, const double K) = 0;
      std::vector<kBestSC> *k_best_2_equiv(const SuperCat *sc, const double K);
      virtual std::vector<kBestSC> *k_best_2(Chart &chart); 
      
      virtual void k_best_3_getCandidates(const SuperCat *sc) = 0;
      virtual void k_best_3_lazynext(std::multiset<kBestSC> &cand, kBestSC kb, const double K) = 0;
      void k_best_3_lazy(const SuperCat *sc, double rank, const double K);
      virtual std::vector<kBestSC> *k_best_3(Chart &chart);
    };
  }
}
