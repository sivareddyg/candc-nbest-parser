// C&C NLP tools
// Copyright (c) Universities of Edinburgh, Oxford and Sydney
// Copyright (c) James R. Curran
//
// This software is covered by a non-commercial use licence.
// See LICENCE.txt for the full text of the licence.
//
// If LICENCE.txt is not included in this distribution
// please email candc@it.usyd.edu.au to obtain a copy.

#include "parser/_decoder.h"

#include <limits>

using namespace std;

namespace NLP { namespace CCG {

const SuperCat *
Decoder::best_equiv(const SuperCat *sc){
  if(sc->max)
    return sc->max;

  const SuperCat *max_sc = 0;
  double max_score = -numeric_limits<double>::max();

  for(const SuperCat *equiv = sc; equiv; equiv = equiv->next){
    double current = best_score(equiv);
    if(current > max_score){
      max_score = current;
      max_sc = equiv;
    }
  }
  
  std::vector<kBestSC> kbv = std::vector<kBestSC>();
  kBestSC kb = kBestSC(max_sc, 0, 0);
  kb.score = max_score;
  kbv.push_back(kb);
  sc->kmax = kbv;

  //FB: This statement failed at (repeatable, but) random-seeming times.
  //    Debug suggested there wasn't actually a problem, and removing it hasn't caused any crashes (yet).
  //FB: Modified sc to no longer store updated score, so this would be useless anyways.
///  assert(max_sc->score == max_score);
  return sc->max = max_sc;
}

std::vector<kBestSC> *
Decoder::best(Chart &chart){
  Cell &root = chart.root();

  const SuperCat *max_sc = 0;
  
  double max_score = -numeric_limits<double>::max();

  for(Cell::iterator i = root.begin(); i != root.end(); ++i){
    best_equiv(*i);
    if((*i)->kmax[0].score > max_score){
      max_score = (*i)->kmax[0].score;
      max_sc = (*i);
    }
  }

  if(max_sc){ // Theoretically possible that root is empty
    max_sc->kmax.at(0).Z = chart.Z;
    return &(max_sc->kmax);
  }
  else {
    return 0; // Necessary to return 0 when no parses have been found
  }
}

// *********** k-best parsing algorithm 0 *************

std::vector<kBestSC>
Decoder::k_merge_0(std::vector<kBestSC> *a, std::vector<kBestSC> *b, const double K){
  std::vector<kBestSC> ret;
  
  std::vector<kBestSC>::iterator a_it = a->begin();
  std::vector<kBestSC>::iterator b_it = b->begin();
  int i = 0;
  
  while(i < K && (a_it != a->end() || b_it != b->end())) {
    if(a_it == a->end()){
      ret.push_back(*b_it);
      ++b_it;
    }
    else if(b_it == b->end()){
      ret.push_back(*a_it);
      ++a_it;
    }
    else if(a_it->score > b_it->score){
      ret.push_back(*a_it);
      ++a_it;
    }
    else {
      ret.push_back(*b_it);
      ++b_it;
    }
    
    ++i;
  }
  
  return ret;
}

std::vector<kBestSC> *
Decoder::k_best_0_equiv(const SuperCat *sc, const double K){
  if(!sc->kmax.empty()) 
    return &sc->kmax;
  
  std::vector<kBestSC> *ret = new std::vector<kBestSC>();

  for(const SuperCat *equiv = sc; equiv; equiv = equiv->next) {
    std::vector<kBestSC> current = k_best_0_score(equiv, K);
    *ret = k_merge_0(ret, &current, K);
  }

  sc->kmax = *ret;
  return &(sc->kmax);
}

std::vector<kBestSC> *
Decoder::k_best_0(Chart &chart){
  Cell &root = chart.root();

  std::vector<kBestSC> ret;

  for(Cell::iterator i = root.begin(); i != root.end(); ++i){
    std::vector<kBestSC> *current = k_best_0_equiv(*i, chart.K);
    ret = k_merge_0(&ret, current, chart.K);
  }
  
  if(ret.size() == 0)
    return 0; // Necessary to return 0 when no parses have been found
  else{
    ret[0].Z = chart.Z;
    chart.kbest = ret;
    return &chart.kbest;
  }
}

// *********** k-best parsing algorithm 1 *************

std::vector<kBestSC> *
Decoder::k_best_1_equiv(const SuperCat *sc, const double K){
  if(!sc->kmax.empty()) 
    return &sc->kmax;
  
  std::vector<kBestSC> *ret = new std::vector<kBestSC>();

  for(const SuperCat *equiv = sc; equiv; equiv = equiv->next) {
    std::vector<kBestSC> current = k_best_1_score(equiv, K);
    *ret = k_merge_0(ret, &current, K); // Merge is the same for this version, so re-use existing code
  }

  sc->kmax = *ret;
  return &(sc->kmax);
}

std::vector<kBestSC> *
Decoder::k_best_1(Chart &chart){
  Cell &root = chart.root();

  std::vector<kBestSC> ret;

  for(Cell::iterator i = root.begin(); i != root.end(); ++i){
    std::vector<kBestSC> *current = k_best_1_equiv(*i, chart.K);
    ret = k_merge_0(&ret, current, chart.K); // Merge is the same for this version, so re-use existing code
  }
  
  if(ret.size() == 0)
    return 0;
  else{
    ret[0].Z = chart.Z;
    chart.kbest = ret;
    return &chart.kbest;
  }
}

// *********** k-best parsing algorithm 2 **********

std::vector<kBestSC> *
Decoder::k_best_2_equiv(const SuperCat *sc, const double K){
  if(!sc->kmax.empty()) 
    return &(sc->kmax);
  
  std::vector<kBestSC> ret;
  std::multiset<kBestSC> cand;

  // Initialize cand with 1-best parse from each member of equiv class
  for(const SuperCat *equiv = sc; equiv; equiv = equiv->next) {
    kBestSC temp = k_best_2_score(equiv, K);
    cand.insert(temp);
  }
  
  while(ret.size() < K && !cand.empty()) {
    // Remove the best candidate and add it to ret
    kBestSC current = *cand.begin();
    ret.push_back(current);
    cand.erase(cand.begin());
    
    // Add that candidate's neighbors to cand
    const SuperCat *csc = current.sc;
    if(csc->left) {
      std::vector<kBestSC> *l = &(csc->left->kmax);
      assert(!l->empty());
      if(csc->right) {
        std::vector<kBestSC> *r = &(csc->right->kmax);
        assert(!r->empty());

        if(current.leftRank + 1 < l->size()) {
          kBestSC next = kBestSC(csc, current.leftRank + 1, current.rightRank);
          next.score = csc->score + (*l)[current.leftRank + 1].score + (*r)[current.rightRank].score;
          // Check to see if next is already in the multiset
          if(cand.count(next) == 0)
            cand.insert(next);
        }
        if(current.rightRank + 1 < r->size()) {
          kBestSC next = kBestSC(csc, current.leftRank, current.rightRank + 1);
          next.score = csc->score + (*l)[current.leftRank].score + (*r)[current.rightRank + 1].score;
          // Check to see if it's already in the multiset
          if(cand.count(next) == 0)
            cand.insert(next);
        }
      }
      else { // Unary rule
        if(current.leftRank + 1 < l->size()) {
          kBestSC next = kBestSC(csc, current.leftRank + 1, current.rightRank);
          next.score = csc->score + (*l)[current.leftRank + 1].score;
          // Check to see if next is already in the multiset
          if(cand.count(next) == 0)
            cand.insert(next);
        }
      }
    }
  }

  sc->kmax = ret;
  return &(sc->kmax);
}

std::vector<kBestSC> *
Decoder::k_best_2(Chart &chart){
  Cell &root = chart.root();

  std::vector<kBestSC> ret;

  for(Cell::iterator i = root.begin(); i != root.end(); ++i){
    std::vector<kBestSC> *current = k_best_2_equiv(*i, chart.K);
    ret = k_merge_0(&ret, current, chart.K); // Merge is the same for this version, so re-use existing code
  }
  
  if(ret.size() == 0)
    return 0; // Necessary to return 0 when no parses have been found
  else{
    ret[0].Z = chart.Z;
    chart.kbest = ret;
    return &chart.kbest;
  }
}

// *********** k-best parsing algorithm 3 **********

void
Decoder::k_best_3_lazy(const SuperCat *sc, double rank, const double K){
  // If cand isn't initialized, add 1-best parse from each member of equiv class
  if(!sc->cand_defined)
    k_best_3_getCandidates(sc);

  while(sc->kmax.size() < rank){
    if(sc->kmax.size() > 0){
      kBestSC next = sc->kmax.at(sc->kmax.size() - 1);
      k_best_3_lazynext(sc->cand, next, K);
    }
    
    if(!sc->cand.empty()){
      kBestSC temp = *sc->cand.begin();
      sc->kmax.push_back(temp);
      sc->cand.erase(sc->cand.begin());
    }
    else
      break;
  }
  
}

std::vector<kBestSC> *
Decoder::k_best_3(Chart &chart){
  chart.kbest.clear();
  Cell &root = chart.root();
  
  std::multiset<kBestSC> cand;
  
  // Init candidate set
  for(Cell::iterator i = root.begin(); i != root.end(); ++i){
    for(const SuperCat *equiv = *i; equiv; equiv = equiv->next){
      kBestSC current = kBestSC(equiv, 0, 0);
      current.score = equiv->score;
      if(equiv->left){
        current.score += equiv->left->kmax[0].score;
        if(equiv->right)
          current.score += equiv->right->kmax[0].score;
      }
      cand.insert(current);
    }
  }
  
  while(chart.kbest.size() < chart.K){
    if(chart.kbest.size() > 0){
      kBestSC next = chart.kbest.at(chart.kbest.size() - 1);
      k_best_3_lazynext(cand, next, chart.K);
    }
    
    if(!cand.empty()){
      kBestSC temp = *cand.begin();
      chart.kbest.push_back(temp);
      cand.erase(cand.begin());
    }
    else
      break;
  }
  
  if(chart.kbest.size() == 0)
    return 0; // Necessary to return 0 when no parses have been found
  else
    chart.kbest[0].Z = chart.Z;
    return &chart.kbest;
}

} }
